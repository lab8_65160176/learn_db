import { AppDataSource } from "./data-source"
import { Product } from "./entity/Product"
import { Type } from "./entity/Type"


AppDataSource.initialize()
.then(async () => {

    const typesRepository = AppDataSource.getRepository(Type)
    const drinkType = await typesRepository.findOneBy({ id: 1 })
    const bekeryType = await typesRepository.findOneBy({ id: 2 })
    const foodType = await typesRepository.findOneBy({ id: 3 })

    const productRepository = AppDataSource.getRepository(Product)
    await productRepository.clear()

    var product = new Product()
    product.id = 1
    product.name = "Americano"
    product.price = 40
    product.type = drinkType
    await productRepository.save(product) 

    product = new Product()
    product.id = 2
    product.name = "milk"
    product.price = 35
    product.type = drinkType
    await productRepository.save(product) 

    
    product = new Product()
    product.id = 3
    product.name = "coffee"
    product.price = 50
    product.type = drinkType
    await productRepository.save(product) 

    product = new Product()
    product.id = 4
    product.name = "green tea"
    product.price = 60
    product.type = drinkType
    await productRepository.save(product) 

    product = new Product()
    product.id = 5
    product.name = "Cakeky"
    product.price = 55
    product.type = bekeryType
    await productRepository.save(product)

    const products = await productRepository.find({ relations: { type:true }})
    console.log(products)
}).
catch(error => console.log(error))
